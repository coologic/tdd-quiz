package com.twuc.webApp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StorageTest {
    private static Storage createStorageWithMaxIntCapacity() {
        return new Storage(Integer.MAX_VALUE);
    }

    @Test
    void should_get_ticket_when_saving_a_bag() {
        Storage storage = createStorageWithMaxIntCapacity();
        Bag bag = new Bag();

        Ticket ticket = storage.save(bag);
        assertNotNull(ticket);
    }

    @Test
    @SuppressWarnings("ConstantConditions")
    void should_get_ticket_when_saving_nothing() {
        Storage storage = createStorageWithMaxIntCapacity();
        Bag nothing = null;

        Ticket ticket = storage.save(nothing);
        assertNotNull(ticket);
    }

    @Test
    void should_get_the_bag_when_retrieving_with_ticked_of_saved_bag() {
        Storage storage = createStorageWithMaxIntCapacity();
        Bag bag = new Bag();
        Ticket ticket = storage.save(bag);

        Bag retrieveBag = storage.retrieve(ticket);
        assertSame(retrieveBag, bag);
    }

    @Test
    @SuppressWarnings("unused")
    void should_get_the_bag_when_retrieving_with_ticked_of_saved_bag_from_two_bag_saved_storage() {
        Storage storage = createStorageWithMaxIntCapacity();
        Bag bag = new Bag();
        Ticket ticket = storage.save(bag);
        Bag otherBag = new Bag();
        Ticket otherBagTicket = storage.save(otherBag);

        Bag retrieveBag = storage.retrieve(ticket);
        assertSame(retrieveBag, bag);
        assertNotSame(retrieveBag, otherBag);
    }

    @Test
    void should_return_null_when_retrieving_with_invalid_ticked_with_empty_storage() {
        Storage emptyStorage = createStorageWithMaxIntCapacity();
        Ticket invalidTicket = new Ticket();

        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> emptyStorage.retrieve(invalidTicket));
        assertEquals("Invalid Ticket", exception.getMessage());
    }

    @Test
    void should_return_null_when_retrieving_with_invalid_ticked_with_have_two_bag_storage() {
        Storage storage = createStorageWithMaxIntCapacity();
        storage.save(new Bag());
        storage.save(new Bag());
        Ticket invalidTicket = new Ticket();

        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> storage.retrieve(invalidTicket));
        assertEquals("Invalid Ticket", exception.getMessage());
    }

    @Test
    @SuppressWarnings("unused")
    void should_return_null_when_double_retrieving_with_invalid_ticked() {
        Storage emptyStorage = createStorageWithMaxIntCapacity();
        Ticket doubleUsedTicket = emptyStorage.save(new Bag());

        Bag firstRetrievedBag = emptyStorage.retrieve(doubleUsedTicket);
        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> emptyStorage.retrieve(doubleUsedTicket));
        assertEquals("Invalid Ticket", exception.getMessage());
    }

    @Test
    @SuppressWarnings("ConstantConditions")
    void should_return_nothing_when_retrieving_with_ticked_of_saved_nothing() {
        Storage storage = createStorageWithMaxIntCapacity();
        Bag nothingBag = null;
        Ticket notingSavedTicked = storage.save(nothingBag);

        Bag retrieveBag = storage.retrieve(notingSavedTicked);
        assertNull(retrieveBag);
    }

    @Test
    @SuppressWarnings("ConstantConditions")
    void should_return_nothing_when_retrieving_with_ticked_of_saved_nothing_in_two_bag_saved_storage() {
        Storage storage = createStorageWithMaxIntCapacity();
        storage.save(new Bag());
        Bag nothingBag = null;
        Ticket notingSavedTicked = storage.save(nothingBag);

        Bag retrievedThing = storage.retrieve(notingSavedTicked);
        assertNull(retrievedThing);
    }

    @Test
    void should_get_ticket_when_save_bag_to_two_capacity_empty_storage() {
        Storage storage = new Storage(2);
        Ticket ticket = storage.save(new Bag());

        assertNotNull(ticket);
    }

    @Test
    void should_save_one_bag_when_save_two_bag_to_one_capacity_empty_storage() {
        Storage storage = new Storage(1);

        Ticket ticket = storage.save(new Bag());
        assertNotNull(ticket);
        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> storage.save(new Bag()));
        assertEquals("Insufficient capacity", exception.getMessage());
    }

    @Test
    void should_return_message_when_save_a_bag_in_two_capacity_full_storage() {
        Storage storage = new Storage(2);
        storage.save(new Bag());
        storage.save(new Bag());

        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> storage.save(new Bag()));
        assertEquals("Insufficient capacity", exception.getMessage());
    }

    @Test
    void should_get_ticket_when_save_a_bag_after_retrieve_a_bag_from_two_capacity_full_storage() {
        Storage storage = new Storage(2);
        storage.save(new Bag(), StorageSlotSize.Small);
        Ticket whateverBagSavedTicket = storage.save(new Bag());

        storage.retrieve(whateverBagSavedTicket);
        Ticket savedBagTicket = storage.save(new Bag());
        assertNotNull(savedBagTicket);
    }

    @Test
    void should_return_ticket_when_save_a_small_bag_to_big_slot() {
        Storage storage = new Storage(0, 0, 1);
        Ticket ticket = storage.save(new Bag(BagSize.Small), StorageSlotSize.Big);

        assertNotNull(ticket);
    }

    @Test
    void should_not_save_a_big_bag_to_small_slot() {
        Storage storage = new Storage(2, 2, 2);

        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> storage.save(new Bag(BagSize.Big), StorageSlotSize.Small));
        assertEquals("Cannot save your bag: Big Small", exception.getMessage());
    }

    @Test
    void should_not_save_a_big_bag_to_no_big_slot_slot() {
        Storage storage = new Storage(1, 1, 0);

        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> storage.save(new Bag(BagSize.Big), StorageSlotSize.Small));
        assertEquals("Cannot save your bag: Big Small", exception.getMessage());
    }

    @Test
    void should_not_save_medium_bag_to_big_slot_with_empty_big_slot_storage() {
        Storage storage = new Storage(1, 1, 0);

        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> storage.save(new Bag(BagSize.Medium), StorageSlotSize.Big));
        assertEquals("Insufficient capacity", exception.getMessage());
    }

    @Test
    void should_save_medium_bag_to_have_empty_medium_slot_storage() {
        Storage storage = new Storage(0, 1, 0);

        Ticket ticket = storage.save(new Bag(BagSize.Medium), StorageSlotSize.Medium);
        assertNotNull(ticket);

    }
}
