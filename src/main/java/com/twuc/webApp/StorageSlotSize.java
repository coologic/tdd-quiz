package com.twuc.webApp;

public enum StorageSlotSize {
    Small(0),
    Medium(1),
    Big(2);
    private final int size;

    StorageSlotSize(int size) {
        this.size = size;
    }

    boolean canSave(Bag bag) {
        return size >= bag.getSize().getSize();
    }
}
