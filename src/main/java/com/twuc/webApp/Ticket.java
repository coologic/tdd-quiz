package com.twuc.webApp;

public class Ticket {
    private final StorageSlotSize size;

    public Ticket() {
        size = null;
    }

    Ticket(StorageSlotSize size) {
        this.size = size;
    }

    StorageSlotSize getSize() {
        return size;
    }
}
