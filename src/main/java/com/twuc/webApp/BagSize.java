package com.twuc.webApp;

public enum BagSize {
    Small(0),
    Medium(1),
    Big(2);
    private final int size;

    BagSize(int size) {
        this.size = size;
    }

    int getSize() {
        return size;
    }
}
