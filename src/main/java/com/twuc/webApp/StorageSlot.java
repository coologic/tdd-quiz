package com.twuc.webApp;

import java.util.HashMap;
import java.util.Map;

class StorageSlot {
    private final int capacity;
    private final Map<Ticket, Bag> slots = new HashMap<>();
    private final StorageSlotSize storageSlotSize;


    StorageSlot(int capacity, StorageSlotSize storageSlotSize) {
        this.capacity = capacity;
        this.storageSlotSize = storageSlotSize;
    }

    Ticket save(Bag bag) {
        if (slots.size() == capacity) {
            throw new IllegalArgumentException("Insufficient capacity");
        }
        if (bag != null && !storageSlotSize.canSave(bag)) {
            throw new IllegalArgumentException(String.format(
                    "Cannot save your bag: %s %s",
                    bag.getSize(),
                    storageSlotSize));
        }
        Ticket ticket = new Ticket(storageSlotSize);
        slots.put(ticket, bag);
        return ticket;
    }

    Bag retrieve(Ticket ticket) {
        if (!slots.containsKey(ticket)) {
            throw new IllegalArgumentException("Invalid Ticket");
        }
        return slots.remove(ticket);
    }

}