package com.twuc.webApp;


import java.util.*;

public class Storage {
    private final Map<StorageSlotSize, StorageSlot> slotMap = new HashMap<>();

    public Storage(Integer smallCapacity) {
        this(smallCapacity, 0, 0);
    }

    public Storage(Integer smallCapacity,
                   Integer medCapacity,
                   Integer bigCapacity) {
        slotMap.put(StorageSlotSize.Small,
                new StorageSlot(smallCapacity,
                        StorageSlotSize.Small));
        slotMap.put(StorageSlotSize.Medium,
                new StorageSlot(medCapacity,
                        StorageSlotSize.Medium));
        slotMap.put(StorageSlotSize.Big,
                new StorageSlot(bigCapacity,
                        StorageSlotSize.Big));
    }

    public Ticket save(Bag bag) {
        return this.save(bag, StorageSlotSize.Small);
    }

    public Ticket save(Bag bag, StorageSlotSize size) {
        StorageSlot storageSlot = slotMap.get(size);
        if (storageSlot == null) {
            throw new IllegalArgumentException("Not Supported Size");
        }
        return storageSlot.save(bag);
    }

    public Bag retrieve(Ticket ticket) {
        if (ticket.getSize() == null) {
            throw new IllegalArgumentException("Invalid Ticket");
        }
        StorageSlot storageSlot = slotMap.get(ticket.getSize());
        if (storageSlot == null) {
            throw new IllegalArgumentException("Invalid Ticket");
        }
        return storageSlot.retrieve(ticket);
    }
}
