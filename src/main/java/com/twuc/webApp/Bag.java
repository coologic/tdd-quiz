package com.twuc.webApp;

public class Bag {
    private BagSize size;

    public Bag() {
        this( BagSize.Small);
    }
    public Bag(BagSize size) {
        this.size = size;
    }

    public BagSize getSize() {
        return size;
    }
}
